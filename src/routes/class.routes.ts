import express from 'express';
import * as classController from '../controllers/class.controller';
import { Schemas, ValidateRequest } from '../middleware/joi';
import { adminOnly } from '../middleware/admin';
import { auth } from '../middleware/auth';

const router = express.Router();

// GET route /api/classes - get all clases (admin sees all details user sees few)
router.get('/', auth, classController.getClasses);

// POST route /api/classes - create new class (admin only)
router.post(
	'/',
	auth,
	adminOnly,
	ValidateRequest(Schemas.classCreate),
	classController.createClass
);

// GET route /api/classes/:id - get single class (admin sees all details user sees few)
router.get('/:id', auth, classController.getClass);

// PUT route /api/classes/:id - update class with id (admin only)
router.put(
	'/:id',
	auth,
	adminOnly,
	ValidateRequest(Schemas.classUpdate),
	classController.updateClass
);

// DELETE route /api/classes/:id - delete class with id (admin only)
router.delete('/:id', auth, adminOnly, classController.deleteClass);

// GET route /api/classes/:id/comments - get comments of class with id (admin only)
router.get(
	'/:id/comments',
	auth,
	adminOnly,
	classController.getCommentsOfClass
);

// POST route /api/classes/:id/comments - add new comment to comments array of class with id
router.post(
	'/:id/comments',
	auth,
	ValidateRequest(Schemas.commentCreate),
	classController.addCommentToClass
);

// GET route /api/classes/:id/ratings - get ratings of class with id (admin only)
router.get('/:id/ratings', auth, adminOnly, classController.getRatingsOfClass);

// POST route /api/classes/:id/ratings - add new rating to ratings array of class with id
router.post(
	'/:id/ratings',
	auth,
	ValidateRequest(Schemas.ratingCreate),
	classController.addRatingToClass
);

// POST route /api/classes/:id/enroll
router.post('/:id/enroll', auth, classController.enrollToClass);

export = router;
