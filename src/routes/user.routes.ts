import express from 'express';
import * as userController from '../controllers/user.controller';
import { Schemas, ValidateRequest } from '../middleware/joi';
import { adminOnly } from '../middleware/admin';
import { auth } from '../middleware/auth';

const router = express.Router();

// GET route /api/users - get all users
router.get('/', auth, adminOnly, userController.getUsers);

// POST route /api/users - create new user
router.post(
	'/',
	auth,
	adminOnly,
	ValidateRequest(Schemas.userCreate),
	userController.createUser
);

// GET route /api/users/:id - get user by id
router.get('/:id', auth, adminOnly, userController.getUser);

// PUT route /api/users/:id - update user with id
router.put(
	'/:id',
	auth,
	adminOnly,
	ValidateRequest(Schemas.userUpdate),
	userController.updateUser
);

// DELETE route /api/users/:id - delete user with id
router.delete('/:id', auth, adminOnly, userController.deleteUser);

// GET route /api/users/:id/verify/:token - verify user email with token generated at register
router.get('/:id/verify/:token', userController.verifyUser);

export = router;
