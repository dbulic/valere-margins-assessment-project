import { Request, Response } from 'express';
import { IUserCreate, IUserUpdate } from '../interfaces/joi';
import UserModel from '../models/user.model';
import VerificationModel from '../models/verification.model';

export const getUsers = (req: Request, res: Response) => {
	// Get all users omit passwords and __v
	UserModel.find()
		.select('-password -__v')
		.then(data => {
			res.status(200).json(data);
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const getUser = (req: Request, res: Response) => {
	// Get user by id omit password and __v
	UserModel.findById(req.params.id)
		.select('-password -__v')
		.then(data => {
			res.status(200).json(data);
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const createUser = (req: Request, res: Response) => {
	// Create new user, body defined in interface IUserCreate (from validation) + verified
	const newUser = new UserModel({
		...(req.body as IUserCreate),
		verified: true,
	});
	newUser
		.save()
		.then(() => {
			res.status(201).json({ message: 'User successfully created!' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const updateUser = (req: Request, res: Response) => {
	// Update user, body defined in interface IUserUpdate (from validation)
	UserModel.updateOne({ _id: req.params.id }, req.body as IUserUpdate)
		.then(() => {
			res.status(200).json({ message: 'User successfully updated!' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const deleteUser = (req: Request, res: Response) => {
	// Delete user by id
	UserModel.deleteOne({ _id: req.params.id })
		.then(() => {
			res.status(200).json({ message: 'User successfully deleted!' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const verifyUser = (req: Request, res: Response) => {
	// Verify user email, get params id and generated token

	VerificationModel.findOne({
		// Find verification record, if found update user
		user: req.params.id,
		verification_code: req.params.token,
	})
		.then(() => {
			return UserModel.updateOne(
				{ _id: req.params.id },
				{ verified: true }
			);
		})
		.then(() => {
			return VerificationModel.deleteOne({
				// if user updated delete verification record
				user: req.params.id,
				verification_code: req.params.token,
			});
		})
		.then(() => {
			// finally, send response
			res.status(200).json({ message: 'User successfully verified!' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};
