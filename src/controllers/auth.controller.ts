import { Request, Response } from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import sgMail from '@sendgrid/mail';

import UserModel from '../models/user.model';
import VerificationModel from '../models/verification.model';
import { IUserCreate } from '../interfaces/joi';

const generateVerToken = () => {
	// generate random string for verification token
	return (
		Math.random().toString(36).slice(2) +
		Math.random().toString(36).slice(2)
	);
};

export const login = (req: Request, res: Response) => {
	// find user with provided email
	UserModel.findOne({ email: req.body.email })
		.then(foundUser => {
			if (!foundUser) {
				throw new Error('Name of user is not correct');
			}
			// compare saved password hash with provided password
			const isMatch = bcrypt.compareSync(
				req.body.password,
				foundUser.password
			);

			if (isMatch) {
				// if password match create token and send to user
				const token = jwt.sign(
					{ _id: foundUser._id?.toString(), email: foundUser.email },
					process.env.SECRET_KEY || 'test_key',
					{
						expiresIn: '2 days',
					}
				);
				res.status(200).json({
					user: { id: foundUser._id, email: foundUser.email },
					token: token,
				});
			} else {
				res.status(401).json({
					message: 'Email or password does not match!',
				});
			}
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const register = (req: Request, res: Response) => {
	// NOTE: if no users in database: first user is by default admin,
	// subsequent users are by default regular users (not admin)

	const token = generateVerToken();
	let userId: string;
	let email: string;
	UserModel.find()
		.select('_id')
		.then(data => {
			const newUser = new UserModel(req.body as IUserCreate);
			if (!data.length) {
				// if no users create user with admin and verified set to true
				newUser.admin = true;
				return newUser.save();
			}
			return newUser.save();
		})
		.then(data => {
			// once user is saved create verification record in database with user id and generated token
			userId = data._id;
			email = data.email;
			const ver = new VerificationModel({
				user: userId,
				verification_code: token,
			});
			return ver.save();
		})
		.then(() => {
			// once the record is stored send verification link to user's email
			sgMail.setApiKey(process.env.SG_API_KEY || '');
			const msg = {
				to: email,
				from: process.env.SG_VERIFIED_SENDER || '', // Change to your verified sender
				subject: 'Email verification',
				text: `Please click on link to verify email: http:${
					process.env.DOMAIN
				}:${
					process.env.PORT || 3000
				}/api/users/${userId}/verify/${token}`,
				html: `Please click on link to verify email: <a href="http:${
					process.env.DOMAIN
				}:${
					process.env.PORT || 3000
				}/api/users/${userId}/verify/${token}">http:${
					process.env.DOMAIN
				}:${
					process.env.PORT || 3000
				}/api/users/${userId}/verify/${token}</a>`,
			};
			return sgMail.send(msg); // TODO: create verification record
		})
		.then(() => {
			// finally send response to user
			res.status(201).json({
				message: 'Created new user! Please verify provided email!',
			});
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};
