/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';
import { IClassCreate, IClassUpdate } from '../interfaces/joi';
import ClassModel from '../models/class.model';

// generate average rating from array of ratings
const getAverageRating = (ratings: number[]) => {
	return ratings.length
		? Math.round(
				(ratings.reduce((acc, num) => acc + num, 0) / ratings.length) *
					100
		  ) / 100
		: null;
};

export const getClasses = (req: Request, res: Response) => {
	// get query params and form object for mongo find filter
	const sports: string[] | undefined = req.query['sport']
		?.toString()
		.split(',');
	const age: string[] | undefined = req.query['age']?.toString().split(',');
	const query: any = {};
	if (sports) query['sport'] = { $in: sports };
	if (age) query['age'] = { $in: age };
	// get all classes that satisfy query params if any, else get all classes
	ClassModel.find(query)
		.select('-__v')
		.then(data => {
			const out: any = data.map(el => {
				return {
					id: el._id,
					sport: el.sport,
					description: el.description,
					rating: getAverageRating(el.ratings),
				};
			});
			res.json(out);
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const createClass = (req: Request, res: Response) => {
	// create new class with data specified in interface IClassCreate
	const newClass = new ClassModel(req.body as IClassCreate);
	newClass
		.save()
		.then(() => {
			res.status(201).json(newClass);
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const getClass = (req: Request, res: Response) => {
	// get class with id from request params
	ClassModel.findById(req.params.id)
		.select('-__v')
		.then(data => {
			if (data) {
				const response: any = {
					sport: data.sport,
					description: data.description,
					schedule: data.schedule,
					age: data.age,
					duration: data.duration,
					rating: getAverageRating(data.ratings),
				};
				if (res.locals.admin) {
					response.ratings = data.ratings;
					response.users = data.users;
					response.comments = data.comments;
				}
				return res.status(200).json(response);
			}
			res.status(500).json({ message: 'No data found! :(' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const updateClass = (req: Request, res: Response) => {
	// update class with id using data specified in interface IClassUpdate
	ClassModel.updateOne({ _id: req.params.id }, req.body as IClassUpdate)
		.then(() => {
			res.status(200).json({ message: 'Class successfully updated' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const deleteClass = (req: Request, res: Response) => {
	// delete class with id
	ClassModel.deleteOne({ _id: req.params.id })
		.then(() => {
			res.status(200).json({ message: 'Class successfully deleted' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const getCommentsOfClass = (req: Request, res: Response) => {
	// get comments of class with id
	ClassModel.findById(req.params.id)
		.select('comments')
		.then(data => {
			res.status(200).json(data?.comments);
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const addCommentToClass = (req: Request, res: Response) => {
	// add new comment to class with id
	ClassModel.updateOne(
		{ _id: req.params.id },
		{ $push: { comments: req.body.comment } }
	)
		.then(() => {
			res.status(201).json({ message: 'Comment successfully added' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const getRatingsOfClass = (req: Request, res: Response) => {
	// get ratings of class with id
	ClassModel.findById(req.params.id)
		.select('ratings')
		.then(data => {
			res.status(200).json(data?.ratings);
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const addRatingToClass = (req: Request, res: Response) => {
	// add new rating to class with id
	ClassModel.updateOne(
		{ _id: req.params.id },
		{ $push: { ratings: req.body.rating } }
	)
		.then(() => {
			res.status(201).json({ message: 'Rating successfully added' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

export const enrollToClass = (req: Request, res: Response) => {
	// add logged-in user to class users array
	const userId = res.locals.token._id;
	ClassModel.find({ users: { $elemMatch: { $eq: userId } } })
		.count()
		.then(data => {
			// check if logged-in user is in less than two classes
			if (data < 2)
				return ClassModel.findById(req.params.id).select('users');
			else
				throw new Error(
					'You have already applied to max amount [2] of classes!'
				);
		})
		.then(data => {
			// check if selected class has less than 10 users
			if (data && data.users.length < 10)
				// if all checks pass add logged-in user to class users array
				return ClassModel.updateOne(
					{ _id: req.params.id },
					{ $push: { users: userId } }
				);
			else throw new Error('No more room in this class!');
		})
		.then(() => {
			res.status(200).json({ message: 'Successfully enrolled!' });
		})
		.catch(err => {
			res.status(403).json({ message: err.message });
		});
};
