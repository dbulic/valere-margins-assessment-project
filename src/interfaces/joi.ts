export interface IUserCreate {
	email: string;
	password: string;
}

export interface IUserUpdate {
	email?: string;
	admin?: boolean;
	verified?: boolean;
}

export interface IClassCreate {
	sport: string;
	description: string;
	schedule: string[];
	age: string;
	duration: string;
}

export interface IClassUpdate {
	sport?: string;
	description?: string;
	schedule?: string[];
	age?: string;
	duration?: string;
	users?: string[];
	ratings?: number[];
	comments?: string[];
}

export interface ICommentCreate {
	comment: string;
}

export interface IRatingCreate {
	rating: number;
}

export interface ILoginRegister {
	email: string;
	password: string;
}
