import { Request, Response, NextFunction } from 'express';
import UserModel from '../models/user.model';

export const adminOnly = (req: Request, res: Response, next: NextFunction) => {
	// check if user has admin rights
	// if true: continue to next middleware
	UserModel.findById(res.locals.token._id).then(data => {
		if (data?.admin) {
			return next();
		}
		res.status(403).json({ message: 'Unauthorised access!' });
	});
};
