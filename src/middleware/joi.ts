import Joi, { ObjectSchema } from 'joi';
import { NextFunction, Request, Response } from 'express';
import {
	IClassCreate,
	IClassUpdate,
	ICommentCreate,
	IRatingCreate,
	IUserCreate,
	IUserUpdate,
} from '../interfaces/joi';

export const ValidateRequest = (schema: ObjectSchema) => {
	return async (req: Request, res: Response, next: NextFunction) => {
		try {
			await schema.validateAsync(req.body);

			next();
		} catch (error) {
			console.error(error);

			return res.status(422).json({ error });
		}
	};
};

export const Schemas = {
	userCreate: Joi.object<IUserCreate>({
		password: Joi.string().required(),
		email: Joi.string().email({ minDomainSegments: 2 }).required(),
	}),
	userUpdate: Joi.object<IUserUpdate>({
		email: Joi.string().email({ minDomainSegments: 2 }),
		admin: Joi.boolean(),
		verified: Joi.boolean(),
	}),
	classCreate: Joi.object<IClassCreate>({
		sport: Joi.string().required(),
		description: Joi.string().required(),
		schedule: Joi.array().items(Joi.string()).required(),
		age: Joi.string()
			.valid('children', 'youth', 'young adults', 'adults')
			.required(),
		duration: Joi.string().required(),
	}),
	classUpdate: Joi.object<IClassUpdate>({
		sport: Joi.string(),
		description: Joi.string(),
		schedule: Joi.array().items(Joi.string()),
		age: Joi.string().valid('children', 'youth', 'young adults', 'adults'),
		duration: Joi.string(),
	}),
	commentCreate: Joi.object<ICommentCreate>({
		comment: Joi.string().required(),
	}),
	ratingCreate: Joi.object<IRatingCreate>({
		rating: Joi.number().less(6).greater(0).required(),
	}),
};
