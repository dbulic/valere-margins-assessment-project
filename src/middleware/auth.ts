import jwt, { JwtPayload } from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import UserModel from '../models/user.model';

export const auth = (req: Request, res: Response, next: NextFunction) => {
	try {
		const token = req.header('Authorization')?.replace('Bearer ', '');
		if (!token) {
			throw new Error();
		}
		const decoded = jwt.verify(token, process.env.SECRET_KEY || 'test_key');
		res.locals.token = decoded;
		UserModel.findById((decoded as JwtPayload)._id).then((data) => {
			if (!data?.verified)
				return res
					.status(403)
					.json({ message: 'Must verify email first!' });
			res.locals.admin = data?.admin || false;
			next();
		});
	} catch (err) {
		res.status(401).json({ message: 'Please login' });
	}
};
