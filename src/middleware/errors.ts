import { Request, Response } from 'express';

export class CustomError extends Error {
	public status: number | undefined;
}

export const handleNotFound = (req: Request, res: Response) => {
	res.status(404).json({ message: 'Not found' });
};
