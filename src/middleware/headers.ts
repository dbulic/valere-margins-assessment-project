import { NextFunction, Request, Response } from 'express';

export const appendHeaders = (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader(
		'Access-Control-Allow-Headers',
		'Origin, X-Requested-With, Content-Type, Accept'
	);
	if (req.method === 'OPTIONS') {
		res.setHeader('Access-Control-Allow-Method', 'GET, POST, PUT, DELETE');
		res.status(200).json({});
	}
	next();
};
