import mongoose from 'mongoose';

export interface VerificationDocument extends mongoose.Document {
	user: string;
	verification_code: string;
}

const VerificationSchema: mongoose.Schema<VerificationDocument> =
	new mongoose.Schema({
		user: { type: String, required: true },
		verification_code: { type: String, required: true },
	});

const VerificationModel = mongoose.model<VerificationDocument>(
	'Verification',
	VerificationSchema
);
export default VerificationModel;
