import mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';

export interface UserDocument extends mongoose.Document {
	email: string;
	password: string;
	admin: boolean;
	verified: boolean;
}

const UserSchema: mongoose.Schema<UserDocument> = new mongoose.Schema({
	email: { type: String, unique: true, required: true },
	password: { type: String, required: true },
	admin: { type: Boolean, default: false },
	verified: { type: Boolean, default: false }
});

const saltRounds = 8;

UserSchema.pre('save', async function (next) {
	// eslint-disable-next-line @typescript-eslint/no-this-alias
	const user = this;
	if (user.isModified('password')) {
		user.password = await bcrypt.hash(user.password, saltRounds);
	}
	next();
});

const UserModel = mongoose.model<UserDocument>('User', UserSchema);
export default UserModel;
