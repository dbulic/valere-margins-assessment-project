import mongoose from 'mongoose';

export interface ClassDocument extends mongoose.Document {
	sport: string;
	description: string;
	schedule: string[];
	age: string;
	duration: string;
	users: string[];
	ratings: number[];
	comments: string[];
}

const ClassSchema: mongoose.Schema<ClassDocument> = new mongoose.Schema({
	sport: { type: String, required: true },
	description: { type: String, required: true },
	schedule: { type: [String], required: true },
	age: { type: String, required: true },
	duration: { type: String, required: true },
	users: { type: [mongoose.Types.ObjectId], default: [] },
	ratings: { type: [Number], default: [] },
	comments: { type: [String], default: [] }
});

const ClassModel = mongoose.model<ClassDocument>('Class', ClassSchema);
export default ClassModel;
