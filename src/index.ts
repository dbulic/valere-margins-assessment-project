import express, { Request, Response } from 'express';
import { connect } from 'mongoose';

import userRoutes from './routes/user.routes';
import classRoutes from './routes/class.routes';
import * as authController from './controllers/auth.controller';
import { handleNotFound } from './middleware/errors';
import { appendHeaders } from './middleware/headers';

const app: express.Application = express();
const port = process.env.PORT || 3000;

connect('mongodb://mongo:27017', {
	user: 'damir',
	pass: 'damirspassword',
})
	.then(() => {
		console.log('Successfully connected to database!');
	})
	.catch(err => {
		console.log(err);
		process.exit(1);
	});

app.use(express.json());

app.use(appendHeaders);

app.get('/', (req: Request, res: Response) => {
	res.send('<h1>Welcome to sports complex REST api</h1>');
});

app.use('/api/classes', classRoutes);
app.use('/api/users', userRoutes);

app.post('/api/login', authController.login);
app.post('/api/register', authController.register);

app.use(handleNotFound);

app.listen(port, () => {
	console.log(`Server is running at http://${process.env.DOMAIN}:${port}`);
});
